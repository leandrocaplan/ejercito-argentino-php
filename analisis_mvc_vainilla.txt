De "index.php" hago una petición POST a "controlador/controlador_login.php"
Este importa el contenido de "modelo/login.php", que incluye a la vez el archivo "modelo/config.php", de forma que intenta acceder a la base de datos,
realizar la conexión a la misma, buscar el xódigo de usuario ingresado y corroborar su contraseña.
Si el inicio de sesión fue exitoso (no hubo errores en la conexión a la base de datos, y las credenciales ingresadas fueron correctas),
mediante la función "header("Location: ../vista/menu_principal.php");", el navegador me redirige a esa página mediante una petición GET.

De "vista/menu_principal.php" hago una petición POST a "controlador/opciones.php", con un solo parámetro: "opcion".
En dicho archivo del controlador, la petición POST me devuelve el contenindo del archivo de vista correspondiente al valor de "opcion" que
elegí en el <select> de 'menu_principal.php', previamente pasandole la información que necesita la vista, obtenida desde la base de datos
mediante los diferentes DAO, definidos en los archivos del modelo, existentes en la carpeta "modelos".
Estos DAO tienen configurados los métodos básicos para realizar un CRUD en las diferentes tablas de la base de datos.

Vemos que en cualquier vista seleccionada a partir de alguna de las opciones del menú principal, en la barra de direcciones del navegador siempre vemos la URL:
"http://localhost:8000/controlador/opciones.php"

Si una vista requiere hacer CRUD sobre mas de una de las tablas, para poder relacionar la información entre ellas, "controlador/opciones.php",
le pasará a la vista correspondiente una instancia de los DAO correspondientes a dichas tablas.
Luego, en los archivos correspondientes a las vistas, se relacionarán los datos de las diferentes tablas, obtenidos mediante cada diferente DAO,
de acuerdo a la funcionalidad que implemente la vista correspondiente.

El método "session_start();" se invoca únicamente desde dos archivos distintos:

 - Desde "modelo/login.php", si la conexión a la base de datos y la verificación de credenciales fue correcta
   Aquí es donde se almacenan los datos del usuario (un militar) en el array "$_SESSION". El contenido del mismo permanecerá intacto mientras dure la sesión

 - Desde "vista/menu_principal.php". En este caso, lo utiliza primero para mostrar la graduación, el nombre y el apellido del usuario (militar) que inició sesión.
   Además, selecciona las opciones disponibles en el menú principal de acuerdo a su graduación, guardada como variable de sesión.

Necesitamos invocar este método en cada archivo ".php" para tener disponible el contenido de $_SESSION desde allí.
El contenido de este array global permanecerá hasta que se invoque al método "session_destroy();", o bien hasta que cerremos el navegador.

"session_destroy();" solo se invoca desde "controlador/opciones.php", cuando recibe la opción "logout" como valor del parámetro "opción", en la petición POST.

En cada archivo de vista se incluye la línea:
 "<p>Haga click <a href="../vista/menu_principal.php">aquí</a> para volver a la página principal</p>"

De esta forma, accedemos a ese archivo mediante una petición GET, de la misma forma que con el método 'header(...)'.

