<?php
	require("../modelo/militares_dao.php");
	$militaresDAO = new MilitaresDAO();
	$militaresDAO->cargarMilitares();
	
	require("../modelo/cuarteles_dao.php");
	$cuartelesDAO = new CuartelesDAO();
	
	$asignada=false;
	$error;

	foreach($militaresDAO->getMilitares() as $militar){
		if($militar['Tipo']=="Soldado"){
			if($militar['Cuartel']==$_POST['cuartelBaja'])
				$asignada=true;
		}
	}
	if(!$asignada) {
		if($cuartelesDAO->bajaCuartel($_POST['cuartelBaja'],$error)){
			$consulta="La baja ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}
	else {
		$error="No se puede dar de baja el cuartel, ya que un soldado lo tiene asignado.";
		require("../vista/error.php");
	}
?>