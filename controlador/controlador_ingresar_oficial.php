<?php
	require("../modelo/militares_dao.php");
	$militaresDAO = new MilitaresDAO();
	$error;
	$militaresDAO->cargarMilitares();
	$existe=false;
	$esOficial=false;
	foreach($militaresDAO->getMilitares() as $militar){
		if($militar['Codigo']==$_POST['codigo']){
			$existe=true;
			if($militar['Tipo']=="Oficial")
				$esOficial=true;
		}
	}
	if(!$existe) {
		if($militaresDAO->ingresarOficial($_POST['codigo'],$_POST['password'],$_POST['nombre'],$_POST['apellido'],$_POST['graduacion'],$error)){
			$consulta="El alta ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}
	else if ($esOficial) {
		if($militaresDAO->modificarOficial($_POST['codigo'],$_POST['password'],$_POST['nombre'],$_POST['apellido'],$_POST['graduacion'],$error)){
			$consulta="La modificación ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}
	else {
		$error="El codigo de militar ingresado existe y no corresponde a un oficial";
		require("../vista/error.php");
	}
?>