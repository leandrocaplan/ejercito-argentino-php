 <?php
	//session_start();
	switch ($_POST['opcion']) {
		case "mostrar_militares";		
			require("../modelo/militares_dao.php");
			$militaresDAO = new MilitaresDAO();
			$militaresDAO->cargarMilitares();
			$militares=$militaresDAO->getMilitares();
      error_log("Entro a 'opciones', case 'mostrar_militares'");
      error_log("Contenido de \$militares:");
      error_log(gettype($militares)); //array
      error_log(json_encode($militares)); //Array JSON con todos los objetos JSON obtenidos a partir de la iteración del objeto 'mysqli_result' en $resultados, por el método 'cargarMilitares'
			//Ver archivo 'resultado_quierie_militares.json' para ver el contenido completo que nos queda en este array.

			require("../modelo/companias_dao.php");
			$companiasDAO = new CompaniasDAO();
			$companiasDAO->cargarCompanias();
			$companias=$companiasDAO->getCompanias();
			
			require("../modelo/cuerpos_dao.php");
			$cuerposDAO = new CuerposDAO();
			$cuerposDAO->cargarCuerpos();
			$cuerpos=$cuerposDAO->getCuerpos();
			
			require("../modelo/cuarteles_dao.php");
			$cuartelesDAO = new CuartelesDAO();
			$cuartelesDAO->cargarCuarteles();
			$cuarteles=$cuartelesDAO->getCuarteles();
			
			require("../modelo/servicios_dao.php");
			$serviciosDAO = new ServiciosDAO();
			$serviciosDAO->cargarServicios();
			$servicios=$serviciosDAO->getServicios();
			//("Location: ../vista/mostrar_militares.php?militares=".$militares);
			require("../modelo/servicios_realizados_dao.php");
			$serviciosRealizadosDAO = new ServiciosRealizadosDAO();
			$serviciosRealizadosDAO->cargarServiciosRealizados();
			$serviciosRealizados=$serviciosRealizadosDAO->getServiciosRealizados();
			
			require_once("verificar_integridad.php");
			
			if(!verificar_integridad_militares($militares,$companias,$cuerpos,$cuarteles)){
				$error="La integridad referencial de la tabla de militares está corrupta.";
				require("../vista/error.php");
			}
			else if(!verificar_integridad_servicios_realizados($militares,$servicios,$serviciosRealizados)){
				$error="La integridad referencial de la tabla de servicios realizados está corrupta.";
				require("../vista/error.php");
			}
			else
				require_once("../vista/mostrar_militares.php");
			break;
		case "mostrar_companias";
			require("../modelo/companias_dao.php");
			$companiasDAO = new CompaniasDAO();
			$companiasDAO->cargarCompanias();
			$companias=$companiasDAO->getCompanias();
			require_once("../vista/mostrar_companias.php");
			break;
		case "mostrar_cuerpos";
			require("../modelo/cuerpos_dao.php");
			$cuerposDAO = new CuerposDAO();
			$cuerposDAO->cargarCuerpos();
			$cuerpos=$cuerposDAO->getCuerpos();
			require_once("../vista/mostrar_cuerpos.php");
			break;
		case "mostrar_cuarteles";
			require("../modelo/cuarteles_dao.php");
			$cuartelesDAO = new CuartelesDAO();
			$cuartelesDAO->cargarCuarteles();
			$cuarteles=$cuartelesDAO->getCuarteles();
			require_once("../vista/mostrar_cuarteles.php");
			break;
		case "mostrar_servicios";
			require("../modelo/servicios_dao.php");
			$serviciosDAO = new ServiciosDAO();
			$serviciosDAO->cargarServicios();
			$servicios=$serviciosDAO->getServicios();
			require_once("../vista/mostrar_servicios.php");
			break;
		case "alta_militar";
			require("../vista/ingresar_militar.php");
			break;
		case "alta_compania";
			require("../vista/ingresar_compania.php");
			break;
		case "alta_cuerpo";
			require("../vista/ingresar_cuerpo.php");
			break;
		case "alta_cuartel";
			require("../vista/ingresar_cuartel.php");
			break;
		case "alta_servicio";
			require("../vista/ingresar_servicio.php");
			break;
		case "baja_militar";
			require("../modelo/militares_dao.php");
			$militaresDAO = new MilitaresDAO();
			$militaresDAO->cargarMilitares();
			$militares=$militaresDAO->getMilitares();
			require_once("../vista/baja_militar.php");
			break;
		case "baja_compania";
			require("../modelo/companias_dao.php");
			$companiasDAO = new CompaniasDAO();
			$companiasDAO->cargarCompanias();
			$companias=$companiasDAO->getCompanias();
			require_once("../vista/baja_compania.php");
			break;
		case "baja_cuerpo";
			require("../modelo/cuerpos_dao.php");
			$cuerposDAO = new CuerposDAO();
			$cuerposDAO->cargarCuerpos();
			$cuerpos=$cuerposDAO->getCuerpos();
			require_once("../vista/baja_cuerpo.php");
			break;
		case "baja_cuartel";
			require("../modelo/cuarteles_dao.php");
			$cuartelesDAO = new CuartelesDAO();
			$cuartelesDAO->cargarCuarteles();
			$cuarteles=$cuartelesDAO->getCuarteles();
			require_once("../vista/baja_cuartel.php");
			break;
		case "baja_servicio";
			require("../modelo/servicios_dao.php");
			$serviciosDAO = new ServiciosDAO();
			$serviciosDAO->cargarServicios();
			$servicios=$serviciosDAO->getServicios();
			require_once("../vista/baja_servicio.php");
			break;
		case "alta_soldado";
			require_once("validar_soldado.php");
		
			if(!$faltaElemento){
				require_once("../vista/ingresar_soldado.php");
			}
			else {
				$error="No se puede dar de alta un soldado, ya que no hay " . $faltante ." disponibles.";
				require_once("../vista/error.php");
			}

			break;
		case "baja_soldado";
			require("../modelo/militares_dao.php");
			$militaresDAO = new MilitaresDAO();
			$militaresDAO->cargarMilitares();
			$militares=$militaresDAO->getMilitares();
			require_once("../vista/baja_soldado.php");
			break;
		case "asignar_servicio";
			require("../modelo/militares_dao.php");
			$militaresDAO = new MilitaresDAO();
			$militaresDAO->cargarMilitares();
			$militares=$militaresDAO->getMilitares();
			
			require("../modelo/servicios_dao.php");
			$serviciosDAO = new ServiciosDAO();
			$serviciosDAO->cargarServicios();
			$servicios=$serviciosDAO->getServicios();
			
			require_once("../vista/asignar_servicio.php");
			break;
		case "desasignar_servicio";
			require("../modelo/militares_dao.php");
			$militaresDAO = new MilitaresDAO();
			$militaresDAO->cargarMilitares();
			$militares=$militaresDAO->getMilitares();
			
			require("../modelo/servicios_dao.php");
			$serviciosDAO = new ServiciosDAO();
			$serviciosDAO->cargarServicios();
			$servicios=$serviciosDAO->getServicios();
			
			require("../modelo/servicios_realizados_dao.php");
			$serviciosRealizadosDAO = new ServiciosRealizadosDAO();
			$serviciosRealizadosDAO->cargarServiciosRealizados();
			$serviciosRealizados=$serviciosRealizadosDAO->getServiciosRealizados();
			
			require_once("../vista/desasignar_servicio.php");
			break;

		case "logout";
			session_destroy();
			header("Location: ../index.php");
			break;
	}
?>