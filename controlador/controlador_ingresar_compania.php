<?php
	require("../modelo/companias_dao.php");
	$companiasDAO = new CompaniasDAO();
	$error;
	$companiasDAO->cargarCompanias();
	$existe=false;	$esOficial=false;
	foreach($companiasDAO->getCompanias() as $compania){
		if($compania['Codigo']==$_POST['codigo'])
			$existe=true;
	
	}
	if(!$existe) {
		if($companiasDAO->ingresarCompania($_POST['codigo'],$_POST['actividad'],$error)){
			$consulta="El alta ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}

	else {
		if($companiasDAO->modificarCompania($_POST['codigo'],$_POST['actividad'],$error)){
			$consulta="La modificación ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}
?>