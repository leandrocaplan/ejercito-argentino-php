<?php
	require("../modelo/militares_dao.php");
	$militaresDAO = new MilitaresDAO();
	
	require("../modelo/servicios_realizados_dao.php");
	$serviciosRealizadosDAO = new ServiciosRealizadosDAO();
	$serviciosRealizadosDAO->cargarServiciosRealizados();
	//$serviciosRealizados=$serviciosRealizadosDAO->getServiciosRealizados();
	$cargado=false;
	$error;

	foreach($serviciosRealizadosDAO->getServiciosRealizados() as $servicioRealizado){
		if($servicioRealizado['CodigoSoldado']==$_POST['militarBaja']){
			$cargado=true;
		}
	}
	if(!$cargado) {
		if($militaresDAO->bajaMilitar($_POST['militarBaja'],$error)){
			$consulta="La baja ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}
	else {
		$error="No se puede dar de baja el soldado, ya que el mismo tiene asignado uno o mas servicios.";
		require("../vista/error.php");
	}
?>