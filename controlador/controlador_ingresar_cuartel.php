<?php
	require("../modelo/cuarteles_dao.php");
	$cuartelesDAO = new CuartelesDAO();
	$error;
	$cuartelesDAO->cargarCuarteles();
	$existe=false;
	foreach($cuartelesDAO->getCuarteles() as $cuartel){
		if($cuartel['Codigo']==$_POST['codigo'])
			$existe=true;
	
	}
	if(!$existe) {
		if($cuartelesDAO->ingresarCuartel($_POST['codigo'],$_POST['nombre'],$_POST['ubicacion'],$error)){
			$consulta="El alta ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}

	else {
		if($cuartelesDAO->modificarCuartel($_POST['codigo'],$_POST['nombre'],$_POST['ubicacion'],$error)){
			$consulta="La modificación ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}
?>