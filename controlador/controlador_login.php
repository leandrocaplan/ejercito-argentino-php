<?php
header('Content-type: text/html; charset=utf-8');
if ((isset($_POST['codigo']) && $_POST['codigo'] != '') && (isset($_POST['password']) && $_POST['password'] != '')) {

  $codigo = $_POST['codigo'];
  $password = $_POST['password'];
  $error_conn;
  require("../modelo/login.php");

  switch (validar_login($codigo, $password, $error_conn)) {
    case 0;
      iconv(mb_detect_encoding($error_conn, mb_detect_order(), true), "UTF-8", $error_conn);
      $error = 'Conexión fallida: ' . $error_conn;
      require "../vista/error_login.php";
      break;
    case 1;
      $error = 'Hubo un error al ejecutar la consulta a la base de datos : ' . $error_conn;
      require "../vista/error_login.php";
      break;
    case 2;
      $error = "Código no encontrado";
      require "../vista/error_login.php";
      break;
    case 3;
      $error = "Contraseña incorrecta";
      require "../vista/error_login.php";
      break;
    case 4;
      error_log(session_status()); //2
      header("Location: ../vista/menu_principal.php");
      break;
  }
}
else {
  $error = "Error: alguno de los campos ingresados está vacío.";
  require "../vista/error_login.php";
}
