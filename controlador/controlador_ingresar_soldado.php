<?php
require("../modelo/militares_dao.php");
$militaresDAO = new MilitaresDAO();
$error;
$militaresDAO->cargarMilitares();
$existe = false;
$esSoldado = false;

foreach ($militaresDAO->getMilitares() as $militar) {
  if ($militar['Codigo'] == $_POST['codigo']) {
    $existe = true;
    if ($militar['Tipo'] == "Soldado")
      $esSoldado = true;
  }
}
if (!$existe) {
  if ($militaresDAO->ingresarSoldado(
    $_POST['codigo'],
    $_POST['password'],
    $_POST['nombre'],
    $_POST['apellido'],
    $_POST['graduacion'],
    $_POST['codCompania'],
    $_POST['codCuerpo'],
    $_POST['codCuartel'],
    $error)) {
      $consulta = "El alta ";
      require("../vista/consulta_exitosa.php");
    } 
  else require("../vista/error.php");
} 
else if ($esSoldado) {
  if ($militaresDAO->modificarSoldado(
    $_POST['codigo'],
    $_POST['password'],
    $_POST['nombre'],
    $_POST['apellido'],
    $_POST['graduacion'],
    $_POST['codCompania'],
    $_POST['codCuerpo'],
    $_POST['codCuartel'],
    $error)) {
      $consulta = "La modificación ";
      require("../vista/consulta_exitosa.php");
    } 
  else require("../vista/error.php");
} 
else {
  $error = "El codigo de militar ingresado existe y no corresponde a un soldado";
  require("../vista/error.php");
}
