<?php
	require("../modelo/companias_dao.php");
	$companiasDAO = new CompaniasDAO();
	$companiasDAO->cargarCompanias();
	$companias=$companiasDAO->getCompanias();
	
	require("../modelo/cuerpos_dao.php");
	$cuerposDAO = new CuerposDAO();
	$cuerposDAO->cargarCuerpos();
	$cuerpos=$cuerposDAO->getCuerpos();
	
	require("../modelo/cuarteles_dao.php");
	$cuartelesDAO = new CuartelesDAO();
	$cuartelesDAO->cargarCuarteles();
	$cuarteles=$cuartelesDAO->getCuarteles();
	
	$faltaElemento=false;
	$faltante;
	
	if(empty($companias)){
		$faltante="compañías";
		$faltaElemento=true;
	}
	else if(empty($cuerpos)){
		$faltante="cuerpos";
		$faltaElemento=true;
	}
	else if(empty($cuarteles)){
		$faltante="cuarteles";
		$faltaElemento=true;
	}
?>