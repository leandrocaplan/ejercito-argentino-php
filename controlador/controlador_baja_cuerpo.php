<?php
	require("../modelo/militares_dao.php");
	$militaresDAO = new MilitaresDAO();
	$militaresDAO->cargarMilitares();
	
	require("../modelo/cuerpos_dao.php");
	$cuerposDAO = new CuerposDAO();
	
	$asignada=false;
	$error;

	foreach($militaresDAO->getMilitares() as $militar){
		if($militar['Tipo']=="Soldado"){
			if($militar['Cuerpo']==$_POST['cuerpoBaja'])
				$asignada=true;
		}
	}
	if(!$asignada) {
		if($cuerposDAO->bajaCuerpo($_POST['cuerpoBaja'],$error)){
			$consulta="La baja ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}
	else {
		$error="No se puede dar de baja el cuerpo, ya que un soldado lo tiene asignado.";
		require("../vista/error.php");
	}
?>