<?php
	require("../modelo/militares_dao.php");
	$militaresDAO = new MilitaresDAO();
	$militaresDAO->cargarMilitares();
	
	require("../modelo/companias_dao.php");
	$companiasDAO = new CompaniasDAO();
	
	$asignada=false;
	$error;

	foreach($militaresDAO->getMilitares() as $militar){
		if($militar['Tipo']=="Soldado"){
			if($militar['Compania']==$_POST['companiaBaja'])
				$asignada=true;
		}
	}
	if(!$asignada) {
		if($companiasDAO->bajaCompania($_POST['companiaBaja'],$error)){
			$consulta="La baja ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}
	else {
		$error="No se puede dar de baja la companía, ya que un soldado la tiene asignada.";
		require("../vista/error.php");
	}
?>