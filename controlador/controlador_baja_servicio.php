<?php
	require("../modelo/servicios_dao.php");
	$serviciosDAO = new ServiciosDAO();
	
	require("../modelo/servicios_realizados_dao.php");
	$serviciosRealizadosDAO = new ServiciosRealizadosDAO();
	$serviciosRealizadosDAO->cargarServiciosRealizados();
	//$serviciosRealizados=$serviciosRealizadosDAO->getServiciosRealizados();
	$cargado=false;
	$error;

	foreach($serviciosRealizadosDAO->getServiciosRealizados() as $servicioRealizado){
		if($servicioRealizado['CodigoServicio']==$_POST['servicioBaja']){
			$cargado=true;
		}
	}
	if(!$cargado) {
		if($serviciosDAO->bajaServicio($_POST['servicioBaja'],$error)){
			$consulta="La baja ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}
	else {
		$error="No se puede dar de baja el servicio, ya que el mismo está asignado a uno o mas soldados.";
		require("../vista/error.php");
	}
?>