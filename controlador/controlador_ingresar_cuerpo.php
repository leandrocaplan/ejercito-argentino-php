<?php
	require("../modelo/cuerpos_dao.php");
	$cuerposDAO = new CuerposDAO();
	$error;
	$cuerposDAO->cargarCuerpos();
	$existe=false;
	foreach($cuerposDAO->getCuerpos() as $cuerpo){
		if($cuerpo['Codigo']==$_POST['codigo'])
			$existe=true;
	
	}
	if(!$existe) {
		if($cuerposDAO->ingresarCuerpo($_POST['codigo'],$_POST['denominacion'],$error)){
			$consulta="El alta ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}

	else {
		if($cuerposDAO->modificarCuerpo($_POST['codigo'],$_POST['denominacion'],$error)){
			$consulta="La modificación ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}
?>