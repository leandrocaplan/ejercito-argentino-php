<?php
	function verificar_integridad_militares($militares,$companias,$cuerpos,$cuarteles){
		$integra=true;
		foreach ($militares as $militar){
			if ($militar['Tipo']=='Soldado'){
				$integra=false;
				if(!empty($companias) && !empty($cuerpos) && !empty($cuarteles)){
					$tiene_compania=false;
					$tiene_cuerpo=false;
					$tiene_cuartel=false;
					foreach ($companias as $compania){
						if($militar['Compania'] == $compania['Codigo'])
							$tiene_compania=true;
					}
					
					foreach ($cuerpos as $cuerpo){
						if($militar['Cuerpo'] == $cuerpo['Codigo'])
							$tiene_cuerpo=true;
					}
					
					foreach ($cuarteles as $cuartel){
						if($militar['Cuartel'] == $cuartel['Codigo'])
							$tiene_cuartel=true;
					}
					if($tiene_compania && $tiene_cuerpo && $tiene_cuartel)
						$integra=true;
				}
			}
			if(!$integra)
				return false;
		}
		return true;
	}
	
	function verificar_integridad_servicios_realizados($militares,$servicios,$serviciosRealizados){
		if(empty($serviciosRealizados))
			return true;		
		
		foreach($serviciosRealizados as $servicioRealizado){
			$integra=false;
			$existe_soldado=false;
			$existe_servicio=false;
			
			foreach ($militares as $militar){
				if ($militar['Tipo']=='Soldado'){
					if($servicioRealizado['CodigoSoldado']==$militar['Codigo'])
						$existe_soldado=true;
					}
			}
		
			foreach ($servicios as $servicio){
				if($servicioRealizado['CodigoServicio']==$servicio['Codigo'])
					$existe_servicio=true;
			}
			if($existe_soldado && $existe_servicio)
				$integra=true;
			
			if(!$integra)
				return false;
		}
		return true;
	}
?>