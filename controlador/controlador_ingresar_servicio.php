<?php
	require("../modelo/servicios_dao.php");
	$serviciosDAO = new ServiciosDAO();
	$error;
	$serviciosDAO->cargarServicios();
	$existe=false;
	foreach($serviciosDAO->getServicios() as $servicio){
		if($servicio['Codigo']==$_POST['codigo'])
			$existe=true;
	
	}
	if(!$existe) {
		if($serviciosDAO->ingresarServicio($_POST['codigo'],$_POST['descripcion'],$error)){
			$consulta="El alta ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}

	else {
		if($serviciosDAO->modificarServicio($_POST['codigo'],$_POST['descripcion'],$error)){
			$consulta="La modificación ";
			require("../vista/consulta_exitosa.php");
		}
		else
			require("../vista/error.php");
	}
?>