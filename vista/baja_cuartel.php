<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Dar de baja un cuartel: </title>
    </head>
    <body>
        <h1>
        Seleccione el cuartel a dar de baja:
        </h1>
        <form method="post" action="controlador_baja_cuartel.php">
            <p>
            <select name="cuartelBaja" size="1" style="width:600px; height:40px" >
                <?php
				
				foreach($cuarteles as $cuartel) {
					echo '<option value="' .$cuartel['Codigo']. '" align="center">'; 
					echo 	'Código: '. $cuartel['Codigo']. '&nbsp; , &nbsp'; 
					echo 	'Nombre: '. $cuartel['Nombre']. '&nbsp; , &nbsp'; 
					echo 	'Ubicacion: '. $cuartel['Ubicacion']; 	 
					echo '</option>';
				}
				?>
            </select>
            <br><br>
            <input type="submit" style="width:60px; height:40px" value ="Enviar">
            </p>
        </form>
		<p>Haga click <a href="../vista/menu_principal.php">aquí</a> para volver a la página principal</p>
    </body>
</html>
