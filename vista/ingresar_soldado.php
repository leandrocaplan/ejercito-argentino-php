<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
		<style>
		th
		{
			text-align: left; 
			vertical-align: middle;
		}
		input,select
		{
			width:350px;
			height:30px;
		}
		</style>
        <title>Ingresar o modificar soldado</title>
    </head>
    <body>
        <h1>
            Ingrese los datos del soldado:
        </h1>
        <h5>
            Si existe un soldado con el código ingresado, se modificará.
            <br>
            Si no existe un soldado con el código ingresado, se agregará.
            
        </h5>
		<br>
        <form method="post" action="../controlador/controlador_ingresar_soldado.php">
            <table align="center">
                <tr align="left">
                    <th>Código:</th>
					<th>&emsp;</th>
                    <th><input type="number" name="codigo"> </th>
				</tr>
				<tr>
					<th>Contraseña:</th>
					<th>&emsp;</th>
					<th> <input type="password" name="password"></th>
				</tr>
				<tr>
					<th>Nombre:</th>
					<th>&emsp;</th>
					<th><input type="text" name="nombre"></th>
				</tr>
				<tr>
					<th>Apellido:</th>
					<th>&emsp;</th>
					<th><input type="text" name="apellido"></th>
				</tr>
				<tr>
					<th>Graduación:</th>
					<th>&emsp;</th>
					<th>
				<select name="graduacion">
					<option value="Voluntario de primera"> Voluntario de primera</option>
					<option value="Voluntario de segunda"> Voluntario de segunda </option>
					<option value="Voluntario de segunda en comision"> Voluntario de segunda en comision</option>
				</select>
					</th>
				</tr>
				<tr>
					<th>Companía: </th>
					<th>&emsp;</th>
					<th>
				<select name="codCompania">
					<?php
						foreach ($companias as $compania){
							echo '<option value="' . $compania['Codigo'] . '" align="center"> ';
							echo 	'Código: '.$compania['Codigo'] .'&nbsp; , &nbsp; ';
							echo 	'Actividad: '.$compania['Actividad'];
						}	echo '</option>';
					?>
				</select>
					</th>
				</tr>

				<tr align="left">
				   
					<th>Cuerpo: </th>
					<th>&emsp;</th>
					<th>
				<select name="codCuerpo">
					<?php
						foreach ($cuerpos as $cuerpo){
							echo '<option value="' . $cuerpo['Codigo'] . '" align="center"> ';
							echo 	'Código: '.$cuerpo['Codigo'] .'&nbsp; , &nbsp; ';
							echo 	'Denominación: '.$cuerpo['Denominacion'];
							echo '</option>';
						}
					?>				
            </select>
                </th>
            </tr>
			
            <tr align="left">
                <th> Cuartel: </th>
                <th>&emsp;</th>
				<th>
            <select name="codCuartel">
                <?php	
					foreach ($cuarteles as $cuartel){
						echo '<option value="' . $cuartel['Codigo'] . '" align="center"> ';
						echo 	'Código: '.$cuartel['Codigo'] .'&nbsp; , &nbsp; ';
						echo 	'Nombre: '.$cuartel['Nombre'];
						echo '</option>';
					}
				?>
            </select>
                </th>
            </tr>
            </table>

<br>
            <input type="submit" style="width:110px; height:40px" value ="Enviar">  
            </p>
        </form> 
	<p>Haga click <a href="../vista/menu_principal.php">aquí</a> para volver a la página principal</p>
    </body>
</html>