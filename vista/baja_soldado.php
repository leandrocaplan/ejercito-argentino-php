<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Ingreso al Sistema del Ejercito Argentino: </title>
    </head>
    <body>
        <h1>
        Seleccione el militar a dar de baja:
        </h1>
        <form method="post" action="controlador_baja_militar.php">
            <select name="militarBaja" size="1" style="width:600px; height:40px" >
                <?php
				foreach($militares as $militar){
					if($militar['Tipo']=="Soldado"){
						echo '<option value="' .$militar['Codigo']. '" align="center">'; 
						echo 'Código: '. $militar['Codigo']. '&nbsp; , &nbsp'; 
						echo 'Tipo: '. $militar['Tipo']. '&nbsp; , &nbsp'; 
						echo 'Nombre: '. $militar['Nombre']. '&nbsp; , &nbsp'; 
						echo 'Apellido: '. $militar['Apellido']; 
						echo '</option>';
					}
				}
				?>
            </select>
            <br><br>
            <input type="submit" style="width:60px; height:40px" value ="Enviar">
        </form>
		<p>Haga click <a href="../vista/menu_principal.php">aquí</a> para volver a la página principal</p>
    </body>
</html>