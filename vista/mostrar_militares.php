<html>

<head>
  <meta charset="UTF-8">
  <title>Mostrar militares</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="../css/estilo.css">
</head>

<body>
  <h1>Muestro Militares</h1>
  <br><br>
  <div class="container lightgreen">
    <table class="table table-borderless">
      <thead class="thead-dark">
        <tr align="center">
          <th scope="col">Código</th>
          <th scope="col">Tipo</th>
          <th scope="col">Nombre</th>
          <th scope="col">Apellido</th>
          <th scope="col">Graduación</th>
        </tr>
      </thead>

      <?php

      foreach ($militares as $militar) {
        echo '<tr align="center">';
        echo "<td>" . $militar['Codigo'] . "</td>";
        echo "<td>" . $militar['Tipo'] . "</td>";
        echo "<td>" . $militar['Nombre'] . "</td>";
        echo "<td>" . $militar['Apellido'] . "</td>";
        echo "<td>" . $militar['Graduacion'];
        "</td>";
        echo "</tr>";
        if ($militar['Tipo'] == 'Soldado') {
          echo '<tr align="center">';
          echo "<div>";
          echo "<td></td>";
          foreach ($companias as $compania) {
            if ($militar['Compania'] == $compania['Codigo'])
              echo "<td>Compañía: " . $compania['Actividad'] . "</td>";
          }
          echo "</div>";

          echo "<div>";
          foreach ($cuerpos as $cuerpo) {
            if ($militar['Cuerpo'] == $cuerpo['Codigo'])
              echo "<td>Cuerpo: " . $cuerpo['Denominacion'] . "</td>";
          }
          echo "</div>";

          echo "<div>";
          foreach ($cuarteles as $cuartel) {
            if ($militar['Cuartel'] == $cuartel['Codigo'])
              echo "<td>Cuartel: " . $cuartel['Nombre'] . "</td>";
          }
          echo "</div>";
          echo "</tr>";
          echo "</th>";
          echo '<tr align="center"><td></td><td></td><td>Servicios Asignados:</td></tr>';



          foreach ($serviciosRealizados as $servicioRealizado) {
            if ($militar['Codigo'] == $servicioRealizado['CodigoSoldado']) {
              echo '<tr align="center">';
              foreach ($servicios as $servicio) {
                if ($servicio['Codigo'] == $servicioRealizado['CodigoServicio']) echo "<td></td><td>Descripción: " . $servicio['Descripcion'] . "</td>";
              }
              echo "<td></td><td>Fecha: " . $servicioRealizado['Fecha'] . "</td>";
              echo '</tr>';
            }
          }

          echo "<th></th>";
          echo "<th></th>";
          echo "<tr></tr>";
          echo "<th></th>";
          echo "<tr></tr>";
          echo "<th></th>";
        }
      }
      ?>

    </table>
  </div>
  <p>Haga click <a href="../vista/menu_principal.php">aquí</a> para volver a la página principal</p>

</body>

</html>