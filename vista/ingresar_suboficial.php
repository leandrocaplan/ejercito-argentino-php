<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
		<style>
		th
		{
			text-align: left; 
			vertical-align: middle;
		}
		input,select
		{
			width:250px;
			height:30px;
		}
		</style>
        <title>Ingresar o modificar suboficial</title>
    </head>
    <body>
        <h1>
            Ingrese los datos del suboficial:
        </h1>
        <h5>
            Si existe un suboficial con el código ingresado, se modificará.
            <br>
            Si no existe un suboficial con el código ingresado, se agregará.

        </h5>
		<br>
        <form method="post" action="../controlador/controlador_ingresar_suboficial.php">
            <p>
            <table style="margin: 0 auto;">
                <tr>
                    <th>Código:</th>
					<th>&emsp;</th>
					<th><input type="number" name="codigo"></th>
                </tr>
                <tr>
                    <th>Contraseña:
					<th>&emsp;</th>
					<th><input type="password" name="password"></th>
                </tr>
                <tr>
                    <th>Nombre:
					<th>&emsp;</th>
					<th><input type="text" name="nombre"></th>
                </tr>
                <tr>
                    <th>Apellido:
					<th>&emsp;</th>
					<th><input type="text" name="apellido"></th>
                </tr>
                <tr>
                    <th>Graduación:</th>
                    <th>&emsp;</th>
					<th>
                <select name="graduacion" size="1">

                    <option value="Suboficial Mayor"> Suboficial Mayor </option>
                    <option value="Suboficial Principal"> Suboficial Principal </option>
                    <option value="Sargento Ayudante"> Sargento Ayudante </option>
                    <option value="Sargento Primero"> Sargento Primero </option>
                    <option value="Sargento"> Sargento </option>
                    <option value="Cabo Primero"> Cabo Primero </option>
                    <option value="Cabo"> Cabo </option>


                </select></th>
                        
                </tr>
            </table>
            <br>
            <input type="submit" style="width:110px; height:40px" value ="Enviar">  
            </p>
        </form> 
		<p>Haga click <a href="../vista/menu_principal.php">aquí</a> para volver a la página principal</p>
    </body>
</html>