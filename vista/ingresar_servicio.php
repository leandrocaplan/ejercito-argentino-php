<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
		<style>
		th
		{
			text-align: left; 
			vertical-align: middle;
		}
		</style>
        <title>Ingresar o modificar servicio</title>
    </head>
    <body>
        <h1>
            Ingrese los datos del servicio:
        </h1>
        <h5>
            Si existe un servicio con el código ingresado, se modificará.
            <br>
            Si no existe un servicio con el código ingresado, se agregará.

        </h5>
		<br>
        <form method="post" action="controlador_ingresar_servicio.php">
            <table style="margin: 0 auto;">
                <tr>
                    <th>Código:</th>
                    <th>&emsp;</th>
					<th><input type="number" name="codigo"></th>
                </tr>
                <tr>
                    <th>Descripción:</th>
                    <th>&emsp;</th>
					<th><input type="text" name="descripcion"></th>
                </tr>
            </table>

            <br>
            <input type="submit" style="width:110px; height:40px" value ="Enviar">  
            </p>
        </form>
	<p>Haga click <a href="../vista/menu_principal.php">aquí</a> para volver a la página principal</p>		
    </body>
</html>
