<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">

        <title>Asignar servicio a un soldado</title>
    </head>
    <body>
        <h1>
            Seleccione los datos del servicio a asignar:
        </h1>

        <form method="post" action="../controlador/controlador_asignar_servicio.php">
            <table style="margin: 0 auto;">
                <tr>
                    <th>Soldado:</th>
                    <th>
                        <select name="codSoldado" size="1" >
						<?php

							foreach($militares as $militar){
								if ($militar['Tipo']=='Soldado'){
									echo '<option value="'.$militar['Codigo'].'" align="center"> ';
                                    echo    'Codigo: '.$militar['Codigo']. '&nbsp;&nbsp';
									echo    'Nombre: '.$militar['Nombre']. '&nbsp;&nbsp';
									echo    'Apellido: '.$militar['Apellido']. '&nbsp;&nbsp';
									echo '</option>';
								}
							}
							?>
                        </select>
						</th>
                </tr>
				
                <tr>
                    <th> Servicio:</th>
                    <th>
                <select name="codServicio" size="1"  >
                    <?php
					foreach($servicios as $servicio){

						echo '<option value="'.$servicio['Codigo'].'" align="center"> ';
						echo    'Codigo: '.$servicio['Codigo']. '&nbsp;&nbsp';
						echo    'Descripción: '.$servicio['Descripcion']. '&nbsp;&nbsp';
						echo '</option>';
						}
					?>
                </select></th>
                </tr>
                <tr>
                    <th>Fecha:</th>
                    <th><input type="date" name="fecha"></th> 
                </tr>
            </table>

            <br>
            <input type="submit" style="width:110px; height:40px" value ="Enviar">  
            </p>
        </form> 

    </body>
</html>