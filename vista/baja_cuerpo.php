<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Dar de baja un cuerpo: </title>
    </head>
    <body>
        <h1>
        Seleccione el cuerpo a dar de baja:
        </h1>
        <form method="post" action="controlador_baja_cuerpo.php">
            <p>
            <select name="cuerpoBaja" size="1" style="width:600px; height:40px" >
                <?php
				
				foreach($cuerpos as $cuerpo) {
					echo '<option value="' .$cuerpo['Codigo']. '" align="center">'; 
					echo 'Código: '. $cuerpo['Codigo']. '&nbsp; , &nbsp'; 
					echo 'Denominación: '. $cuerpo['Denominacion']; 	 
					echo '</option>';
				}
				?>
            </select>
            <br><br>
            <input type="submit" style="width:60px; height:40px" value ="Enviar">
            </p>
        </form>
		<p>Haga click <a href="../vista/menu_principal.php">aquí</a> para volver a la página principal</p>
    </body>
</html>
