<!DOCTYPE HTML>
<html>
    <head> 
        <title>Menu principal</title> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="Expires" content="0">
		<meta http-equiv="Last-Modified" content="0">
		<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
		<meta http-equiv="Pragma" content="no-cache">
		<link rel="stylesheet" type="text/css" href="../css/estilo.css">
    </head>
    <body>
	<?php
		session_start();
		echo "<h1>¡Bienvenido! Su ingreso fue exitoso</h1>";
		echo "<h2>Usted ingresó como: ";
		echo $_SESSION['Graduacion']." ";
		echo $_SESSION['Nombre']." ";
		echo $_SESSION['Apellido']."</h2>";
		echo "<br>";
		
		echo <<<EOT
		<form method="post" action="../controlador/opciones.php">
			<p>Sus opciones son:&emsp;
        <select name="opcion" size="1" style="width:300px; height:40px ">
          <option value="mostrar_militares">Mostrar militares</option>
          <option value="mostrar_companias">Mostrar compañías</option>
          <option value="mostrar_cuerpos">Mostrar cuerpos</option>
          <option value="mostrar_cuarteles">Mostrar cuarteles</option>
          <option value="mostrar_servicios">Mostrar servicios</option>
EOT;	
		if ($_SESSION['Tipo'] == "Oficial"){
		echo <<<EOT
          <option value="alta_militar"> Dar de alta o modificar un militar</option>
          <option value="alta_compania"> Dar de alta o modificar una compañía</option>
          <option value="alta_cuerpo"> Dar de alta o modificar un cuerpo</option>
          <option value="alta_cuartel"> Dar de alta o modificar un cuartel</option>
          <option value="alta_servicio"> Dar de alta o modificar un servicio</option>
          <option value="baja_militar"> Dar de baja un militar</option>
          <option value="baja_compania"> Dar de baja una compañía</option>
          <option value="baja_cuerpo"> Dar de baja un cuerpo</option>
          <option value="baja_cuartel"> Dar de baja un cuartel</option>
          <option value="baja_servicio"> Dar de baja un servicio</option>
          <option value="asignar_servicio"> Asignar servicio a un soldado</option>
          <option value="desasignar_servicio"> Dar de baja el servicio a un soldado</option>
				
EOT;
		}
		
		else if ($_SESSION['Tipo'] == "Suboficial"){
		echo <<<EOT
          <option value="alta_soldado"> Dar de alta o modificar un soldado</option>
          <option value="alta_servicio"> Dar de alta o modificar un servicio</option>
          <option value="baja_soldado"> Dar de baja un soldado</option>
          <option value="baja_servicio"> Dar de baja un servicio</option>
          <option value="asignar_servicio"> Asignar servicio a un soldado</option>
          <option value="desasignar_servicio"> Dar de baja el servicio a un soldado</option>
                    
EOT;
		}
    else if ($_SESSION['Tipo'] != "Soldado") echo "<p>Error</p>";

		
		echo <<<EOT
		      <option value="logout"> Logout</option>
			  </select>
			  <br><br>
        <input type="submit" style="width:60px; height:40px" value ="Enviar">
      </p>
    </form>	
EOT;			
		?>
		<!--
		<p>Haga click <a href="index.php">aquí</a> para salir</p>
		-->
    </body>   
</html>