<html>
    <head>
        <meta charset="UTF-8">
        <title>Mostrar cuarteles</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    </head>
    <body>
		<h1>Muestro Cuarteles</h1>
		<br><br>
		<div class="container lightgreen">
            <table class="table table-borderless">
                <center>
				<thead class="thead-dark">
                    <tr align="center">
                        <th scope="col">Código</th>
                        <th scope="col">Nombre</th>
						<th scope="col">Ubicación</th>
                    </tr>
                </thead>

		<?php
		foreach ($cuarteles as $cuartel){
			echo '<tr align="center">';
			echo '<td>'.$cuartel['Codigo'].'</td>';
		    echo '<td>'.$cuartel['Nombre'].'</td>';
			echo '<td>'.$cuartel['Ubicacion'].'</td>';
			echo "</tr>";
		}
		?>
		</table>
		</div>
	<p>Haga click <a href="../vista/menu_principal.php">aquí</a> para volver a la página principal</p>
	</body>
</html>