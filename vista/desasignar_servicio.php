<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Desasignar servicio: </title>
    </head>
    <body>
        <h1>
        Seleccione el servicio a desasignar:
        </h1>

        <form method="post" action="controlador_desasignar_servicio.php">
            <select name="servicioDesasignado" size="1" style="width:900px; height:40px" al	ign="center">
                <?php
				
				foreach($serviciosRealizados as $servicioRealizado){
					echo '<option value="'.$servicioRealizado['CodigoServicioRealizado'].'" align="center"> ';
					//echo $servicioRealizado['CodigoServicioRealizado'];
					foreach($servicios as $servicio){
						if($servicio['Codigo']==$servicioRealizado['CodigoServicio']){
							echo 'Codigo de servicio: '.$servicio['Codigo'].'&nbsp;&nbsp';
                            echo 'Descripcion: '.$servicio['Descripcion'].'&nbsp;&nbsp';
							echo 'Fecha: '.$servicioRealizado['Fecha'].'&nbsp;&nbsp';
						}
					}
					echo '&nbsp;&nbsp;&nbsp;&nbsp;';
					
					foreach($militares as $militar){
						if($militar['Codigo']==$servicioRealizado['CodigoSoldado']){
							echo 'Codigo de soldado: '.$militar['Codigo'].'&nbsp;&nbsp';
                            echo 'Nombre: '.$militar['Nombre'].'&nbsp;&nbsp';
							echo 'Apellido: '.$militar['Apellido'].'&nbsp;&nbsp';
						}
					}
					
					echo '</option>';
				}
				?>
            </select>
            <br><br>

            <input type="submit" style="width:60px; height:40px" value ="Enviar">
        </form>
		<p>Haga click <a href="../vista/menu_principal.php">aquí</a> para volver a la página principal</p>    
    </body>
</html>