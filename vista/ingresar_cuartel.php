<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
		<style>
		th
		{
			text-align: left; 
			vertical-align: middle;
		}
		</style>
        <title>Ingresar o modificar cuartel</title>
    </head>
    <body>
        <h1>
            Ingrese los datos del cuartel:
        </h1>
        <h5>
            Si existe un cuartel con el código ingresado, se modificará.
            <br>
            Si no existe un cuartel con el código ingresado, se agregará.

        </h5>
		<br>
        <form method="post" action="controlador_ingresar_cuartel.php">

            <table style="margin: 0 auto;">
                <tr>
                    <th>Código:</th>
                    <th>&emsp;</th>
					<th> <input type="number" name="codigo"> </th>
                </tr>
                <tr>
                    <th>Nombre:</th>
                    <th>&emsp;</th>
					<th><input type="text" name="nombre"></th>
                </tr>
                <tr>
                    <th>Ubicación:</th>
                    <th>&emsp;</th>
					<th> <input type="text" name="ubicacion"></th>
                </tr>
                
            </table>
            <br>
            <input type="submit" style="width:110px; height:40px" value ="Enviar">  

        </form> 
		<p>Haga click <a href="../vista/menu_principal.php">aquí</a> para volver a la página principal</p>
    </body>
</html>