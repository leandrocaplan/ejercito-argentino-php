<html>
    <head>
        <meta charset="UTF-8">
        <title>Mostrar companias</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    </head>
    <body>
		<h1>Muestro Compañías</h1>
		<br><br>
		<div class="container lightgreen">
            <table class="table table-borderless">
                <center>
				<thead class="thead-dark">
                    <tr align="center">
                        <th scope="col">Código</th>
                        <th scope="col">Actividad</th>
 
                    </tr>
                </thead>

		<?php
		foreach ($companias as $compania){
			echo '<tr align="center">';
			echo '<td>'.$compania['Codigo'].'</td>';
		    echo '<td>'.$compania['Actividad'].'</td>';
			echo "</tr>";
		}
		?>
		</table>
		</div>
	<p>Haga click <a href="../vista/menu_principal.php">aquí</a> para volver a la página principal</p>
	</body>
</html>