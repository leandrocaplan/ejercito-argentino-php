<html>
    <head>
        <meta charset="UTF-8">
        <title>Mostrar servicios</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    </head>
    <body>
		<h1>Muestro Servicios</h1>
		<br><br>
		<div class="container lightgreen">
            <table class="table table-borderless">
                <center>
				<thead class="thead-dark">
                    <tr align="center">
                        <th scope="col">Código</th>
                        <th scope="col">Descripción</th>						
                    </tr>
                </thead>

		<?php
		foreach ($servicios as $servicio){
			echo '<tr align="center">';
			echo '<td>'.$servicio['Codigo'].'</td>';
		    echo '<td>'.$servicio['Descripcion'].'</td>';
			echo "</tr>";
		}
		?>
		</table>
		</div>
	<p>Haga click <a href="../vista/menu_principal.php">aquí</a> para volver a la página principal</p>
	</body>
	</body>
</html>