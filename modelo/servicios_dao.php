<?php
class ServiciosDAO{
	private $servicios;
	private $db;
	public function __construct() {
			require("config.php");
			$this->servicios = array();
			$this->db=$conn;

		}

	public function cargarServicios(){
		$sql="SELECT * FROM servicios";
		$resultados = $this->db->query($sql);
		foreach ($resultados as $resultado){
			$this->servicios[]=$resultado;
		}	
	}
	public function ingresarServicio($codigo,$descripcion,&$error){
	
		$sql ="INSERT INTO servicios(Codigo,Descripcion) VALUES (" . $codigo . ",'" . $descripcion . "')";
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}	
	}

	public function modificarServicio($codigo,$descripcion,&$error){
	
		$sql ="UPDATE servicios SET Descripcion = '" . $descripcion . "' WHERE Codigo=" . $codigo;
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}	
	}
	
	public function bajaServicio($codigo,&$error){
		$sql="DELETE FROM servicios WHERE codigo=" . $codigo;
		//echo $sql;
		//echo $this->db->query($sql);
		
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}
	}

	public function getServicios(){
		return $this->servicios;
	}
}
?>