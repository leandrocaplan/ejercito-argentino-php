<?php
class CompaniasDAO{
	private $companias;
	private $db;
	public function __construct() {
			require("config.php");
			$this->companias = array();
			$this->db=$conn;

		}

	public function cargarCompanias(){
		$sql="SELECT * FROM companias";
		$resultados = $this->db->query($sql);
		foreach ($resultados as $resultado){
			$this->companias[]=$resultado;
		}	
	}
	
	public function ingresarCompania($codigo,$actividad,&$error){
	
		$sql ="INSERT INTO companias(Codigo,Actividad) VALUES (" . $codigo . ",'" . $actividad . "')";
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}	
	}

	public function modificarCompania($codigo,$actividad,&$error){
	
		$sql ="UPDATE companias SET  Actividad = '" . $actividad . "' WHERE Codigo=" . $codigo;
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}	
	}
	
	public function bajaCompania($codigo,&$error){
		$sql="DELETE FROM companias WHERE codigo=" . $codigo;
		//echo $sql;
		//echo $this->db->query($sql);
		
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}
	}
	
	public function getCompanias(){
		return $this->companias;
	}
}
?>