<?php
class ServiciosRealizadosDAO{
	private $serviciosRealizados;
	private $db;
	public function __construct() {
			require("config.php");
			$this->db=$conn;

		}

	public function cargarServiciosRealizados(){
		$sql="SELECT * FROM serviciosrealizados";
		$resultados = $this->db->query($sql);
		foreach ($resultados as $resultado){
			$this->serviciosRealizados[]=$resultado;
		}	
	}
	
	public function asignarServicio($codSoldado,$codServicio,$fecha,&$error){
	
		$sql = "INSERT INTO serviciosrealizados(CodigoSoldado,CodigoServicio,Fecha)  VALUES (" . $codSoldado . "," . $codServicio . ",'" . $fecha . "')";
		//echo $sql;
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}	
	}

	public function desasignarServicio($codigo,&$error){
		$sql="DELETE FROM serviciosrealizados WHERE CodigoServicioRealizado=" . $codigo;
		//echo $sql;
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}
	}

	public function getServiciosRealizados(){
		return $this->serviciosRealizados;
	}
}
?>