<?php
class CuerposDAO{
	private $cuerpos;
	private $db;
	public function __construct() {
			require("config.php");
			$this->companias = array();
			$this->db=$conn;

		}

	public function cargarCuerpos(){
		$sql="SELECT * FROM cuerpos";
		$resultados = $this->db->query($sql);
		foreach ($resultados as $resultado){
			$this->cuerpos[]=$resultado;
		}	
	}
	
	public function ingresarCuerpo($codigo,$denominacion,&$error){
	
		$sql ="INSERT INTO cuerpos(Codigo,Denominacion) VALUES (" . $codigo . ",'" . $denominacion . "')";
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}	
	}

	public function modificarCuerpo($codigo,$denominacion,&$error){
	
		$sql ="UPDATE cuerpos SET Denominacion = '" . $denominacion . "' WHERE Codigo=" . $codigo;
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}	
	}
	public function bajaCuerpo($codigo,&$error){
		$sql="DELETE FROM cuerpos WHERE codigo=" . $codigo;
		//echo $sql;
		//echo $this->db->query($sql);
		
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}
	}

	public function getCuerpos(){
		return $this->cuerpos;
	}
}
?>