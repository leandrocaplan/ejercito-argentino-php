<?php
class CuartelesDAO{
	private $cuarteles;
	private $db;
	public function __construct() {
			require("config.php");
			$this->cuarteles = array();
			$this->db=$conn;

		}

	public function cargarCuarteles(){
		$sql="SELECT * FROM cuarteles";
		$resultados = $this->db->query($sql);
		foreach ($resultados as $resultado){
			$this->cuarteles[]=$resultado;
		}	
	}
	public function ingresarCuartel($codigo,$nombre,$ubicacion,&$error){
	
		$sql ="INSERT INTO cuarteles(Codigo,Nombre,Ubicacion) VALUES (" . $codigo . ",'" . $nombre ."','". $ubicacion."')";
		//echo $sql;
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}	
	}

	public function modificarCuartel($codigo,$nombre,$ubicacion,&$error){
		$sql = "UPDATE cuarteles SET Nombre = '" . $nombre . "',Ubicacion = '" . $ubicacion . "' WHERE Codigo=" . $codigo;
	
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}	
	}
	
	public function bajaCuartel($codigo,&$error){
		$sql="DELETE FROM cuarteles WHERE codigo=" . $codigo;
		//echo $sql;
		//echo $this->db->query($sql);
		
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}
	}

	public function getCuarteles(){
		return $this->cuarteles;
	}
}
?>