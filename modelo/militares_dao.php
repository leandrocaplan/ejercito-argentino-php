<?php
class MilitaresDAO{
	private $militares;
	private $db;
	public function __construct() {
			require("config.php");
			$this->militares = array();
			$this->db=$conn;

		}

	public function ingresarOficial($codigo, $password, $nombre, $apellido, $graduacion, &$error) {
		$tipo = "Oficial";
		$sql = "INSERT INTO militares (Codigo,Password,Tipo,Nombre,Apellido,Graduacion)"
				. " VALUES (" . $codigo . ",'" . $password . "','" . $tipo . "','" . $nombre . "','" . $apellido
				. "','" . $graduacion . "')";
		//echo $sql;
		//echo $this->db->query($sql);
		
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}
	}
	public function modificarOficial($codigo, $password, $nombre, $apellido, $graduacion, &$error) {
	
	$tipo = "Oficial";
	$sql = "UPDATE militares SET "
			. "Password = '" . $password . "'"
			. ",Tipo ='" . $tipo . "'"
			. ",Nombre='" . $nombre . "'"
			. ",Apellido='" . $apellido . "'"
			. ",Graduacion='" . $graduacion . "'"
			. "WHERE Codigo=" . $codigo;
		
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}
	}
	
	public function ingresarSuboficial($codigo, $password, $nombre, $apellido, $graduacion, &$error) {
		$tipo = "Suboficial";
		$sql = "INSERT INTO militares (Codigo,Password,Tipo,Nombre,Apellido,Graduacion)"
				. " VALUES (" . $codigo . ",'" . $password . "','" . $tipo . "','" . $nombre . "','" . $apellido
				. "','" . $graduacion . "')";
		//echo $sql;
		//echo $this->db->query($sql);
		
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}
	}
	public function modificarSuboficial($codigo, $password, $nombre, $apellido, $graduacion, &$error) {
	
	$tipo = "Suboficial";
	$sql = "UPDATE militares SET "
			. "Password = '" . $password . "'"
			. ",Tipo ='" . $tipo . "'"
			. ",Nombre='" . $nombre . "'"
			. ",Apellido='" . $apellido . "'"
			. ",Graduacion='" . $graduacion . "'"
			. "WHERE Codigo=" . $codigo;
		
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}
	}
	
	public function ingresarSoldado($codigo, $password, $nombre, $apellido, $graduacion,$codCompania,$codCuerpo,$codCuartel, &$error) {
		$tipo = "Soldado";
		$sql = "INSERT INTO 	militares (Codigo,Password,Tipo,Nombre,Apellido,Graduacion,Compania,Cuerpo,Cuartel)"
				. " VALUES (" . $codigo . ",'" . $password . "','" . $tipo . "','" . $nombre . "','" . $apellido
				. "','" . $graduacion . "'," . $codCompania . "," . $codCuerpo . "," . $codCuartel. ")";
		//echo $sql;
		//echo $this->db->query($sql);
		
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}
	}
	
	public function modificarSoldado($codigo, $password, $nombre, $apellido, $graduacion,$codCompania,$codCuerpo,$codCuartel, &$error) {
		$tipo = "Soldado";
        $sql = "UPDATE militares SET "
				. "Password = '" . $password . "'"
				. ",Tipo ='" . $tipo . "'"
				. ",Nombre='" . $nombre . "'"
				. ",Apellido='" . $apellido . "'"
				. ",Graduacion='" . $graduacion . "'"
				. ",Compania=" . $codCompania  
				. ",Cuerpo=" . $codCuerpo
				. ",Cuartel=" . $codCuartel
				. " WHERE Codigo=" . $codigo;
		//echo $sql;
		//echo $this->db->query($sql);
		
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}
	}

	public function bajaMilitar($codigo,&$error){
		$sql="DELETE FROM militares WHERE codigo=" . $codigo;
		//echo $sql;
		//echo $this->db->query($sql);
		
		if($this->db->query($sql))
			return true;
		else {
			$error=$this->db->error;
			return false;
		}
	}
	public function cargarMilitares(){
		$sql="SELECT * FROM militares";
		$resultados = $this->db->query($sql);
    error_log("Entro a cargar_militares");
    error_log("Tipo de objeto en \$resultados");
    error_log(get_class($resultados)); //mysqli_resut
    error_log(print_r($resultados,true)); //mysqli_resut
		foreach ($resultados as $resultado){ //Una instancia de 'mysqli_resut' resulta ser iterable con un 'foreach'
      error_log("Tipo de objeto en \$resultado");
      error_log(gettype($resultado)); //array
      error_log(json_encode($resultado));
      //Tengo un array asociativo por cada iteración en este foreach. Por ejemplo:
      /*
      {
        "Codigo": "55",
        "Password": "aaa",
        "Tipo": "Soldado",
        "Nombre": "Matias",
        "Apellido": "Lopez",
        "Graduacion": "Voluntario de primera",
        "Compania": "44",
        "Cuerpo": "343",
        "Cuartel": "88",
        "ServiciosRealizados": null
      }
      */
			$this->militares[]=$resultado;
		}	
	}

	public function getMilitares(){
		return $this->militares;
	}
}
?>