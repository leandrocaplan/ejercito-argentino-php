# Sistema de administración del ejercito argentino.

Esta es una pequeña aplicación de ejemplo donde usamos PHP vainilla (sin frameworks), utilizando MySQL/MariaDB como base de datos, en donde definimos manualmente una estructura basada enel patrón MVC (Modelo - Vista - Controlador).

Para ejecutarla, es pertinente tener instalado en el sistema una versión de PHP 8.0 o mayor (En mi caso, utilizo localmente PHP 8.3).
Tammién es necesario tener instalada y caonfigurada alguna versión reciente de MySQL o MariaDB (preferentemente MySQL >5.7 o MariaDB >10.6).

Si bien es una aplicación muy simple, con un maquetado muy básico y un funcionamiento muy elemental, consistente en las operaciones de CRUD mas elementales, ejemplifica de forma satisfactoria la implementación de una aplicación PHP que interactúa con una base de datos MySQL.

Utilizando un servidor PHP, es posible visualizar desde la consola del sistema operativo infromación pertinente al funcionamiento y utilización de las clases y objetos mas elementales que nos permiten establecer una conexión con la base de datos, y la forma en que realiza las quieries pertinentes.

Se encuentran también implementadas en la aplicación los métodos necesarios para asegurar en todo momento la persistencia de la integridad referencial de la base de datos, impidiendo así que ante un error del usuario, la misma se corropma, dejando inutilizable al sistema.

## Pasos para la ejecución en un servidor local

- Debemos asegurarnos de tener instalado localmente una versión de PHP 8.0 o superior.

- Asimismo, debemos asegurarnos de tener instalada localmente alguna version compatible de MySQL o bien de MariaDB. Debemos tener configurado un usuario con totales privilegios para el acceso al motor de base de datos, y conocer sus credenciales.

- Luego, debemos clonar el contenido del repositorio a un directorio local.

- Ya sea desde la terminal de MySQL/MariaDB, o bien de herramientas como phpMyAdmin, debemos importar el archivo 'ejercito.sql', de forma que nos genere la base de datos junto con la estructura de tablas correspondiente, generando también las claves primarias de cada tabla, las restricciones de FK pertinentes, y nos inserte cierto contenido predefinido en estas tablas que definimos, de modo que ya estaremos listos para utilizarla a partir de dicho contenido generado a partir de este archivo que importamos. Podremos hacerlo desde la terminal, o bien desde una interfaz como phpMyAdmin.

- En el archivo './modelo/config.php', debemos colocar las credenciales de nuestra base de datos. Por defecto, la base de datos que utilizaremos se llamará 'ejercito'.

- Una vez clonado el contenido del repositorio en nuestro directorio local, ya teniendo definida nuestra base de datos con sus tablas correspondientes, desde la terminal del sistema operativo nos debemos situar en el directorio raíz de la aplicación y ejecutar el comando 'php -S localhost:8000'. De esta forma, podremos acceder a la aplicación ingresando 'localhost:8000' en la barra de direcciones de nuestro navegador, y visualizar toda la información de debug desde nuestra terminal.

- Podremos ingresar por primera vez al sistema con las credenciales : '1234' como Código, y 'qwerty' como Contraseña. De esta forma, podremos tener acceso a todas las funcionalidades de la aplicación.

##Adicion Jenkinsfile
